# Dusty avocado

Simple chat

## Development environment

- React Native (0.66.4)
- React (17.0.2)
- React Redux (7.2.6)
- Serverless (2.45.2)

## Connected services

- AWS Gateway (WebSockets)
- AWS Lambda

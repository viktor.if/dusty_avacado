const AWS = require('aws-sdk');

const CONNECTION_ENDPOINT = 'xu45wql445.execute-api.us-east-1.amazonaws.com/dev/'
const client = new AWS.ApiGatewayManagementApi({ endpoint: CONNECTION_ENDPOINT });

const names = {};

const sendToOne = async (id, body) => {
    try {
        await client.postToConnection({
            'ConnectionId': id,
            'Data': Buffer.from(JSON.stringify(body)),
        }).promise();
    } catch (error) {
        console.log({ error})
    }
};

const sendToAll = async (ids, body) => {
    const all = ids.map((i) => sendToOne(i, body));
    
    return Promise.all(all)
};

exports.handler = async (event) => {
    if (event.requestContext) {
        const connectionId = event.requestContext.connectionId;
        const routeKey = event.requestContext.routeKey;
        
        let body = {};
        
        try {
            if (event.body) {
                body = JSON.parse(event.body);
            }
        } catch (error) {
            console.log({ error });
        };
        
        switch (routeKey) {
            case '$connect':
                names[connectionId] = body.name;
                break;
                
            case '$disconnect':
                await sendToAll(Object.keys(names), { systemMessage: `${names[connectionId]} has left the chat` });
                delete names[connectionId];
                await sendToAll(Object.keys(names), { members: Object.values(names) });
                break;
                
            case '$default':
                // code
                break;
                
            case 'setName':
                names[connectionId] = body.name;
                await sendToAll(Object.keys(names), { members: Object.values(names) });
                await sendToAll(Object.keys(names), { systemMessage: `${names[connectionId]} has joined the chat` });
                break;
                
            case 'sendMessage':
                await sendToAll(Object.keys(names), { message: `${names[connectionId]}: ${body.message}` });
                break;
            
            default:
                // code
        }
    }
    
    
    // TODO implement
    const response = {
        statusCode: 200,
        body: JSON.stringify('Hello from Lambda!'),
    };
    return response;
};
import createSagaMiddleware from '@redux-saga/core';

import {configureStore, combineReducers} from '@reduxjs/toolkit';

import {rootSaga} from './saga';
import slice from './duck';

const sagaMiddleware = createSagaMiddleware();

const defaultMiddlewareConfig = {
  thunk: false,
};

const rootReducer = combineReducers({
  chat: slice.reducer,
});

export const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware => {
    const middlewares = getDefaultMiddleware(defaultMiddlewareConfig).concat(
      sagaMiddleware,
    );

    return middlewares;
  },
  devTools: true,
});

export type StoreState = ReturnType<typeof store.getState>;

sagaMiddleware.run(rootSaga);

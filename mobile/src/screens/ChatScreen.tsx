import React, {useEffect, useState} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {StyleSheet, Text, TextInput, ScrollView, View} from 'react-native';
import moment from 'moment';

import slice from '../duck';
import {useAppDispatch, useAppSelector} from '../hooks';

export default () => {
  const dispatch = useAppDispatch();
  const {messages, userName} = useAppSelector(state => state.chat);

  const [message, setMessage] = useState<string>('');

  const onSubmitEditing = () => {
    if (message) {
      dispatch(slice.actions.sendMessage({text: message}));
      setMessage('');
    }
  };

  useEffect(() => {
    return () => {
      dispatch(slice.actions.closeConnection());
    };
  }, [dispatch]);

  return (
    <SafeAreaView style={styles.screen}>
      <ScrollView style={{transform: [{scaleY: -1}]}}>
        {messages.map((item, index) => {
          return (
            <View
              key={index}
              style={[
                styles.message,
                item.author === userName
                  ? styles.messageMine
                  : item.author === 'system'
                  ? styles.messageSystem
                  : undefined,
              ]}>
              {item.author !== 'system' && (
                <Text style={styles.messageAuthor}>{item.author}</Text>
              )}
              <View
                style={[
                  styles.messageContainer,
                  item.author === userName && styles.messageContainerMine,
                ]}>
                <Text
                  style={[
                    styles.messageText,
                    item.author === userName && styles.messageTextMine,
                  ]}>
                  {item.text}
                </Text>
              </View>
              {item.author !== 'system' && (
                <Text style={styles.messageTime}>
                  {moment().format('HH:mm')}
                </Text>
              )}
            </View>
          );
        })}
      </ScrollView>
      <TextInput
        style={styles.input}
        placeholder="Type message here..."
        placeholderTextColor="#ccc"
        onChangeText={text => setMessage(text)}
        onSubmitEditing={onSubmitEditing}
        value={message}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  input: {
    borderTopWidth: 1,
    borderColor: '#ccc',
    color: 'dimgray',
    fontSize: 16,
    paddingHorizontal: 8,
  },
  message: {
    maxWidth: '60%',
    transform: [{scaleY: -1}],
    alignSelf: 'flex-start',
  },
  messageMine: {
    alignSelf: 'flex-end',
  },
  messageSystem: {
    alignSelf: 'center',
    marginTop: 8,
  },
  messageContainer: {
    margin: 8,
    marginTop: 2,
    marginBottom: 2,
    borderWidth: 1,
    padding: 12,
    borderRadius: 12,
    borderColor: '#ccc',
  },
  messageContainerMine: {
    backgroundColor: 'maroon',
    borderWidth: 0,
  },
  messageAuthor: {
    color: 'dimgray',
    fontSize: 14,
    fontWeight: '500',
    flexDirection: 'row',
    marginLeft: 8,
  },
  messageText: {
    color: 'dimgray',
    fontSize: 12,
    fontWeight: '500',
    flexDirection: 'row',
    marginHorizontal: 8,
  },
  messageTextMine: {
    color: '#fff',
  },
  messageTime: {
    color: 'dimgray',
    fontSize: 12,
    fontWeight: '300',
    flexDirection: 'row',
    alignSelf: 'flex-end',
    marginHorizontal: 8,
    marginBottom: 8,
  },
});

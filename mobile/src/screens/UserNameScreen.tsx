import {useNavigation} from '@react-navigation/core';
import React, {useState} from 'react';
import {StyleSheet, Text, TextInput, TouchableOpacity} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {StackNavigationProp} from '@react-navigation/stack';
import {useDispatch} from 'react-redux';

import {RootStackPages} from '../navigators/routes';
import {RootStackParamList} from '../navigators/types';
import slice from '../duck';

export default () => {
  const dispatch = useDispatch();
  const [name, setName] = useState<string>('');

  const navigation =
    useNavigation<
      StackNavigationProp<RootStackParamList, RootStackPages.UserNameScreen>
    >();

  const onStartPress = () => {
    navigation.push(RootStackPages.ChatScreen);
    dispatch(slice.actions.openConnection({userName: name}));
  };

  return (
    <SafeAreaView style={styles.screen}>
      <TextInput
        style={styles.input}
        placeholder="Enter your name here..."
        placeholderTextColor="#ccc"
        value={name}
        onChangeText={text => setName(text)}
      />
      <TouchableOpacity
        style={styles.button}
        onPress={onStartPress}
        disabled={!name}>
        <Text style={styles.buttonText}>Start</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  input: {
    borderWidth: 1,
    borderColor: '#ccc',
    color: 'gray',
    fontSize: 16,
    marginHorizontal: 16,
    borderRadius: 8,
    paddingHorizontal: 16,
  },
  button: {
    backgroundColor: 'maroon',
    marginHorizontal: 16,
    marginVertical: 24,
    paddingVertical: 16,
    borderRadius: 8,
  },
  buttonText: {
    fontSize: 14,
    color: '#fff',
    textAlign: 'center',
    textTransform: 'uppercase',
  },
});

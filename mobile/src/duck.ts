import {CaseReducer, createSlice, PayloadAction} from '@reduxjs/toolkit';

export type State = {
  messages: {
    text: string;
    author: string;
  }[];
  userName?: string;
};

const initialState: State = {
  messages: [],
};
export type OpenConnectionAction = PayloadAction<{
  userName: string;
}>;

export type OpenConnection = CaseReducer<State, OpenConnectionAction>;
export type CloseConnection = CaseReducer<State>;

export type ReceiveMessageAction = PayloadAction<{
  data: string;
}>;

export type ReceiveMessage = CaseReducer<State, ReceiveMessageAction>;

export type SendMessageAction = PayloadAction<{
  text: string;
}>;

export type SendMessage = CaseReducer<State, SendMessageAction>;

const openConnection: OpenConnection = (state, {payload}) => {
  state.userName = payload.userName;
};

const closeConnection: CloseConnection = state => {
  state.messages = [];
};

const receiveMessage: ReceiveMessage = (state, {payload}) => {
  const response = JSON.parse(payload.data);

  if (response.systemMessage) {
    state.messages.unshift({text: response.systemMessage, author: 'system'});
  }

  if (response.message) {
    const [author, text] = response.message.split(': ');
    state.messages.unshift({
      text,
      author,
    });
  }
};

const sendMessage: SendMessage = () => undefined;

const reducers = {
  openConnection,
  closeConnection,
  receiveMessage,
  sendMessage,
};

export default createSlice({name: 'chat', initialState, reducers});

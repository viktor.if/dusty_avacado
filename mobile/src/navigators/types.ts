import {RootStackPages} from './routes';

export type RootStackParamList = {
  [RootStackPages.UserNameScreen]: undefined;
  [RootStackPages.ChatScreen]: undefined;
};

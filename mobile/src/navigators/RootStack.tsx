import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {SafeAreaProvider} from 'react-native-safe-area-context';

import UserNameScreen from '../screens/UserNameScreen';
import ChatScreen from '../screens/ChatScreen';
import {StatusBar} from 'react-native';
import {RootStackParamList} from './types';
import {RootStackPages} from './routes';

const Stack = createStackNavigator<RootStackParamList>();

export default function App() {
  return (
    <SafeAreaProvider>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />
      <NavigationContainer>
        <Stack.Navigator initialRouteName={RootStackPages.UserNameScreen}>
          <Stack.Screen
            name={RootStackPages.UserNameScreen}
            component={UserNameScreen}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name={RootStackPages.ChatScreen}
            component={ChatScreen}
            options={{headerShown: false}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
}

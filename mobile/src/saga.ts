import {eventChannel, END} from '@redux-saga/core';
import {
  cancelled,
  put,
  call,
  take,
  takeLatest,
  cancel,
  fork,
} from 'redux-saga/effects';
import slice from './duck';

const socketServerURL =
  'wss://xu45wql445.execute-api.us-east-1.amazonaws.com/dev';
let socket: WebSocket;

const connect = (): Promise<WebSocket> => {
  return new Promise(resolve => {
    resolve(new WebSocket(socketServerURL));
  });
};

const createSocketChannel = (_socket: WebSocket): any =>
  eventChannel(emit => {
    const handler = (data: any) => {
      emit(data);
    };

    const intervalId = setInterval(() => {
      console.log('state: ', _socket.readyState);
      if (_socket.readyState === WebSocket.CLOSED) {
        emit({readyState: WebSocket.CLOSED});
        emit(END);
      }
    }, 60000);

    _socket.onmessage = handler;

    return () => {
      clearInterval(intervalId);
    };
  });

export const listenServerSaga = function* ({
  payload: {userName},
  type,
}: ReturnType<typeof slice.actions.openConnection>): any {
  console.log('listenServerSaga started');
  try {
    socket = yield call(connect);

    socket.onopen = () =>
      socket.send(JSON.stringify({action: 'setName', name: userName}));

    const socketChannel: any = yield call(createSocketChannel, socket);

    while (true) {
      const payload = yield take(socketChannel);

      if (payload.readyState === WebSocket.CLOSED) {
        yield fork(listenServerSaga, {payload: {userName}, type});
      } else {
        yield put(slice.actions.receiveMessage({data: payload.data}));
      }
    }
  } finally {
    if (yield cancelled()) {
      console.log('cancelled');
    }
  }
};

export const stopListeningServerSaga = function* (): any {
  console.log({socket});
  if (socket) {
    socket.close();
  }
};

export const sendMessageSaga = function* ({
  payload,
}: ReturnType<typeof slice.actions.sendMessage>): any {
  if (socket) {
    socket.send(JSON.stringify({action: 'sendMessage', message: payload.text}));
  }
};

export const rootSaga = function* () {
  yield takeLatest(slice.actions.sendMessage, sendMessageSaga);
  yield takeLatest(slice.actions.openConnection, listenServerSaga);
  yield takeLatest(slice.actions.closeConnection, stopListeningServerSaga);
};

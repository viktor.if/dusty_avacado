import {useDispatch, useSelector, TypedUseSelectorHook} from 'react-redux';
import {StoreState, store} from './store';

type AppDispatch = typeof store.dispatch;

/** Dispatch */
export const useAppDispatch = () => useDispatch<AppDispatch>();

/** Selector */
export const useAppSelector: TypedUseSelectorHook<StoreState> = useSelector;
